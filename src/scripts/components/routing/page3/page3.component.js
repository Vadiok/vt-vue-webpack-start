import { mapActions } from "vuex";

export default {
	data() {
		return {
			message: "Страница 3",
		};
	},
	methods: mapActions({
		toLower: "user/surnameToLower",
		toUpper: "user/surnameToUpper",
	}),
	template: `<div>
			<p><strong>{{ message }}</strong></p>
			<p>Компонент находится в js файле, в отличие от других, которые находятся в ts.</p>
			<p>
				Фамилию "{{ $store.state.user.surname }}" в верхний регистр
				<a href="#" class="btn btn-default" @click.prevent="toLower()">
					в нижний регистр
				</a>
				<a href="#" class="btn btn-default" @click.prevent="toUpper()">
					в верхний регистр
				</a>
			</p>
		</div>`,
};
