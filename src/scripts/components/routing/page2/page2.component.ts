/// <reference path="./../../../require.d.ts"/>
import * as Vue from "vue";
import { mapActions } from "vuex";

require("./style.scss");

export default {
	data() {
		return {
			message: "Страница 2",
		};
	},
	template: <string>require("./template.pug"),
	methods: {
		...mapActions({
			toLower: "user/nameToLower",
			toUpper: "user/nameToUpper",
		}),
	},
} as Vue.ComponentOptions<Vue>;
