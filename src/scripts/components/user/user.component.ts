import * as Vue from "vue";
import { mapGetters, mapActions } from "vuex";

export default {
	data() {
		return {
			message: "Пример компонента для всех страниц",
		};
	},
	computed: {
		...mapGetters({
			fullName: "user/fullName",
		}),
	},
	methods: {
		...mapActions({}),
	},
	template: `<div>
		<p><strong>{{ message }}</strong></p>
		<p>Пользователь {{ fullName }}</p>
		<p><input type="text" v-model="$store.state.user.name"> <input type="text" v-model="$store.state.user.surname"></p>
</div>`,
} as Vue.ComponentOptions<Vue>;
