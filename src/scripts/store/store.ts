/// <reference path="../require.d.ts"/>
import * as Vue from "vue";
import * as Vuex from "vuex";
import user from "./modules/user/user.store";

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		inputs: {
			testString: "Some Test String",
		},
		// inputString: "Some Test String",
	},
	modules: {
		user,
	},
});
