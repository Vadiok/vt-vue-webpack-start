import * as Vuex from "vuex";
import * as getters from "./getters";
import * as mutations from "./mutations";
import * as actions from "./actions";

const state = {
	name: "Vlad",
	surname: "Tokarev",
};


export default {
	namespaced: true,
	state,
	getters: <any>getters,
	mutations: <any>mutations,
	actions: <any>actions,
} as Vuex.Module<any, any>;
