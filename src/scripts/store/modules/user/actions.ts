export const nameToLower = (state) => state.commit("changeCase", {field: "name", "case": "lower"});

export const nameToUpper = ({ commit }) => commit("changeCase", {field: "name", "case": "upper"});

export const surnameToLower = ({ commit }) => commit("changeCase", {field: "surname", "case": "lower"});

export const surnameToUpper = ({ commit }) => commit("changeCase", {field: "surname", "case": "upper"});

export default {};
