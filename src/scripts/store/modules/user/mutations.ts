export const changeCase = (state, payload: any) => {
	let newValue = state[payload.field];
	if (typeof newValue !== "string") return;
	if (["lower", "upper"].indexOf(payload.case) === -1) return;
	if (payload.case === "lower") {
		state[payload.field] = newValue.toLowerCase();
	} else {
		state[payload.field] = newValue.toUpperCase();
	}
};

export default {};
