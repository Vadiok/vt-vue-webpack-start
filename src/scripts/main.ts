/// <reference path="./require.d.ts"/>
import * as Vue from "vue";
import { router } from "./router";
import store from "./store/store";

let app = new Vue({
	el: "#my-app",
	data: {
		appHeader: `Пример Vue с lazy load!`,
	},
	components: {
		"all-pages-component": resolve => {
			require.ensure([], () => resolve(require("./components/user/user.component")["default"]));
		},
	},
	router,
	store,
});


// Optional
window["vm"] = app;


// Ajax example - remove it
require.ensure([], function (require) {

	const axios = require("axios");
	const config = {
		headers: {"X-Requested-With": "XMLHttpRequest"},
		transformRequest: [function (data) { return data; }],
		transformResponse: [function (data) { return data; }],
	};
	axios.get(window.location.href + "#test").then(function (data) {
		console.log(data);
	});

	axios.patch("example-data/post-test.php", {
		firstName: "Fred",
		lastName: "Flintstone",
	}).then(function (response) {
		console.log(response.data);
	});

});
