/// <reference path="./require.d.ts"/>
import * as Vue from "vue";
import * as VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
	{
		path: "/page1",
		component: (resolve => require.ensure([], () => resolve(require("./components/routing/page1/page1.component")["default"]))),
	},
	{
		path: "/page2",
		name: "page2",
		component: (resolve => require.ensure([], () => resolve(require("./components/routing/page2/page2.component")["default"]))),
	},
	{
		path: "/page3",
		component: (resolve => require.ensure([], () => resolve(require("./components/routing/page3/page3.component")["default"]))),
	},
	{
		path: "/page4",
		component: (resolve => require.ensure([], () => resolve(require("./components/routing/page4/page4.component.vue")))),
	},
];

export const router = new VueRouter({routes});
