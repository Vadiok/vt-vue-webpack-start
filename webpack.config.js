const path              = require("path");
const argv              = require("yargs")["argv"];
const webpack           = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");

let scriptSources = require("./tasks/script-sources");
let styleSources  = require("./tasks/style-sources");
scriptSources.vendor = require("./tasks/vendor-sources");

let compress = (typeof argv.p !== "undefined" && argv.p);

let extractStyles = new ExtractTextPlugin({filename: "[name].css", allChunks: true,});

let config = [
	{
		context: path.resolve(__dirname, "src/scripts"),
		entry: scriptSources,
		output: {
			path: path.resolve(__dirname, "dist/js"),
			publicPath: "dist/js/",
			filename: "[name].bundle.js",
		},
		resolve: {
			extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js",],
			alias: {
				"vue$": "vue/dist/vue" + (compress ? ".min" : "") + ".js",
				"vue-router$": "vue-router/dist/vue-router.min.js",
				"vuex$": "vuex/dist/vuex.min.js",
			},
		},
		module: {
			rules: [
				{enforce: "pre", test: /\.js$/, use: "source-map-loader", exclude: /node_modules/,},
				{test: /\.js$/, loader: "babel-loader", exclude: [/node_modules/,], query: {presets: ["latest",],}},
				{test: /\.tsx?$/, loader: "ts-loader", exclude: [/node_modules/,],},
				{test: /\.vue$/, loader: "vue-loader", exclude: [/node_modules/,],},
				{test: /\.html?$/, loader: "html-loader", exclude: [/node_modules/,],},
				{test: /\.pug$/, loaders: ["raw-loader", "pug-html-loader"], exclude: [/node_modules/,],},
				{test: /\.scss$/, loaders: ["style-loader", "css-loader", "autoprefixer-loader", "sass-loader",]},
				{test: /\.less$/, loaders: ["style-loader", "css-loader", "autoprefixer-loader", "less-loader",]},
			]
		},
		externals: {
			// axios: "axios",
		},
		plugins: [
			new webpack.optimize.CommonsChunkPlugin({
				name: "vendor",
				filename: "vendor.bundle.js",
				minChunks: Infinity,
			}),
		],
		devtool: "source-map",
	},
	{
		context: path.resolve(__dirname, "src/global-styles"),
		entry: styleSources,
		output: {
			path: path.resolve(__dirname, "dist/css"),
			filename: "[name].bundle.js",
		},
		resolve: {
			extensions: [".s[ac]ss", ".less", ".css",]
		},
		module: {
			rules: [
				{
					test: /\.css$/,
					loader: extractStyles.extract(["css-loader?sourceMap", "autoprefixer-loader",]),
				},
				{
					test: /\.s[ac]ss$/,
					loader: extractStyles.extract(["css-loader?sourceMap", "autoprefixer-loader", "sass-loader?sourceMap",]),
				},
				{
					test: /\.less$/,
					loader: extractStyles.extract(["css-loader?sourceMap", "autoprefixer-loader", "less-loader?sourceMap",]),
				},
			]
		},
		plugins: [
			extractStyles,
		],
		devtool: "source-map",
	},
];

module.exports = config;
